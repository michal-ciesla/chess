package pl.enovic.engine.board;

public class BoardUtils {

    public static final boolean[] FIRST_COLUMNS = null;
    public static final boolean[] SECOND_COLUMNS = null;
    public static final boolean[] SEVENTH_COLUMNS = null;
    public static final boolean[] EIGHTH_COLUMNS = null;

    private BoardUtils() {
        throw new RuntimeException("You can not initialize me");
    }
    public static boolean isValidTileCoordinate(int coordinate) {
        return coordinate>=0 && coordinate<=64;
    }
}
