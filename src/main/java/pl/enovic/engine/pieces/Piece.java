package pl.enovic.engine.pieces;

import pl.enovic.engine.Alliance;
import pl.enovic.engine.board.Board;
import pl.enovic.engine.board.Move;

import java.util.Collection;

public abstract class Piece {

    protected final int piecePosition;
    protected final Alliance pieceAllians;

    Piece(final int piecePosition, final Alliance pieceAllians) {
        this.piecePosition = piecePosition;
        this.pieceAllians = pieceAllians;
    }

    public Alliance getPieceAllians() {
        return pieceAllians;
    }

    public abstract Collection<Move> calculateLegalMoves(final Board board);

}
